extern crate fog;

fn main() {
    fog::Engine::new()
        .start_with("./examples/minimal.yml")
        .run();
}
