/* ---Gloabal variables--- */
var inventory = [];
var commandList = [];
var isCommandRunning = false;
var currentOptions = null;
var isAboutToQuit = false;
var currentColor = null;

/* ---Smaller Functions--- */
function setRoom(room) {
    $("#room").text(room);

    runNextCommand();
}

function setInventory() {
    //TODO: too much for now 

    runNextCommand();
}

function writeText(text) {
    var span = document.createElement("SPAN");
    span.innerText = text;

    // Add color
    if (currentColor != null) {
        span.className += ' ' + currentColor;
    }

    content.appendChild(span); 

    runNextCommand();
}

function setColor(color) {
    if (color == "default" || color == "standard") {currentColor = null}
    else{
        currentColor = color;
    }

    runNextCommand();
}

function showInventory() {

}

function showAbout() {

}

function showOptions(optionList) {
    currentOptions = optionList;
    var div = document.createElement("DIV");
    div.setAttribute("id","options");
    var list = document.createElement("OL");

    for (var i = 0; i < optionList.length; i++) {
        var node = document.createElement("LI");      
        node.innerText = optionList[i];
        node.className = "option active";
        node.setAttribute("onclick","selectedOption(" + i + ")");
        list.appendChild(node);
    }

    div.appendChild(list);
    content.appendChild(div);

    runNextCommand();
}
//showOptions(["This is the first option", "This the second", "And suprise suprise there is the third","four", "five", "six"]);

function selectedOption(id) {
    // Delte the current div
    var div = document.getElementById("options");
    div.innerHTML = "";
    var list = document.createElement("OL");


    for (var i = 0; i < currentOptions.length; i++) {
        var node = document.createElement("LI");      
        node.innerText = currentOptions[i];
        node.className = "option";
        if (i == id) {
            node.className += " selected";
        }
        list.appendChild(node);
    }

    div.appendChild(list);
    div.removeAttribute("id");



    // Tell the backend that a option was selected 
    var msg = {action: "decission", option: id};
    sendObjectToRust(msg);
}

function showEnd() {
    var div = document.createElement("DIV");
    div.innerHTML = '<button type="button" onclick="exitGame()" class="btn btn-primary">Quit Game</button>';
    div.className += " text-center";
    content.appendChild(div); 

    isAboutToQuit = true;

    // Don't call runNextCommand here because this is the end of the game
}

function exitGame() {
    var msg = {action: "exit"};
    sendObjectToRust(msg);
}

//TODO: maybe a bigger element like a card or a jumbotron
function writeError(text) {
    var div = document.createElement("DIV");
    div.innerText = text;
    div.className += " red";
    content.appendChild(div); 

    showEnd();

    // Don't call runNextCommand here because this is the end of the game
}

function sleep(duration) {
    setTimeout(runNextCommand, duration);
}

/* ---Bigger Functions--- */ 
function sendObjectToRust(obj) {
    var text = JSON.stringify(obj);
    window.external.invoke(text);
}

function addCommand(input) {

    commandList.push(JSON.parse(input));

    // Run the next command if it is currently
    if (isCommandRunning === false) {
        runNextCommand();
    }
}

function runNextCommand() {
    // Check if there is even anything to do
    if (commandList.length == 0 || isAboutToQuit === true) {
        isCommandRunning = false;
        return;
    } else {
        isCommandRunning = true;
    }

    var obj = commandList.shift();

    // Parse the object 
    if (obj.action == "write") {
        writeText(obj.text);

    } else if (obj.action == "color") {
        setColor(obj.color);

    } else if (obj.action == "sleep") {
        sleep(obj.duration);
        
    } else if (obj.action == "end") {
        showEnd();
        
    } else if (obj.action == "room") {
        setRoom(obj.roomName);
        
    } else if (obj.action == "error") {
        writeError(obj.text);
        
    } else if (obj.action == "decission") {
        showOptions(obj.optionList);

    } else {
        writeError("ERROR: Unknown frontend command\nthis should not have happened");
        showEnd();
    }
}

function start() {
    const msg = {action: "start"};
    sendObjectToRust(msg);
}
start();
