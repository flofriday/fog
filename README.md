# fog
Flos obvious game-engine.


`fog` is a very simple game-engine for simple texted based games. Games created with fog are more
like [Gamebooks](https://en.wikipedia.org/wiki/Gamebook) and not 
[interactive fiction](https://en.wikipedia.org/wiki/Interactive_fiction) games like 
[Zork](https://en.wikipedia.org/wiki/Zork).

## Desclaimer
At the moment is `fog` not ready to be used. But come back in a couple of days to check it out 
again
