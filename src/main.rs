extern crate serde_json;
extern crate web_view;
extern crate yaml_rust;

mod engine;
mod html;

use serde_json::Value;
use std::sync::{Arc, Mutex};
use std::thread::{sleep_ms, spawn};
use web_view::*;

fn main() {
    let size = (800, 600);
    let HTML = html::create();
    let resizable = true;
    let debug = false;
    let initial_userdata = engine::GameState::from_file("./story/start.yml");
    run(
        "Game",
        Content::Html(HTML),
        Some(size),
        resizable,
        debug,
        move |webview| {},
        move |webview, arg, userdata| {
            let json_obj: Value =
                serde_json::from_str(arg).expect("Did not receive valid json from the frontend");
            match json_obj["action"]
                .as_str()
                .expect("Recived invalid json from frontend")
            {
                "start" => {
                    engine::start(userdata, webview);
                }
                "decission" => {
                    engine::continue_with_choice(
                        userdata,
                        webview,
                        json_obj["option"]
                            .as_u64()
                            .expect("Recived unexpected json from frontend"),
                    );
                }
                "exit" => {
                    webview.terminate();
                }
                _ => unimplemented!(),
            }
        },
        initial_userdata,
    );
}
