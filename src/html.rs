fn inline_style(s: &str) -> String {
    format!(r#"<style type="text/css">{}</style>"#, s)
}

fn inline_script(s: &str) -> String {
    format!(r#"<script type="text/javascript">{}</script>"#, s)
}

fn create_styles() -> String {
    let mut out = String::new();
    out += &inline_style(include_str!("../website/css/bootstrap.css"));
    out += &inline_style(include_str!("../website/index.css"));
    out
}

fn create_scripts() -> String {
    let mut out = String::new();
    out += &inline_script(include_str!("../website/js/jquery-slim.min.js"));
    out += &inline_script(include_str!("../website/js/bootstrap.js"));
    out += &inline_script(include_str!("../website/index.js"));
    out
}

pub fn create() -> String {
    format!(
        r#"
		<!DOCTYPE html>
		<html>
                        <head>
                                <meta charset="UTF-8">
				{styles}
			</head>
			<body>
                                {content}
				{scripts}
			</body>
		</html>
		"#,
        styles = create_styles(),
        content = include_str!("../website/index.html"),
        scripts = create_scripts()
    )
}
