use engine::GameState;
use serde_json::*;
use std::io;
use std::io::stdout;
use std::io::Write;
use std::process;
use web_view::*;

#[derive(Debug)]
pub enum RenderColor {
    Standard,
    Red,
    Green,
    Blue,
    Orange,
    Purple,
}

#[derive(Debug)]
pub enum RenderCommand {
    Text(String),
    Sleep(f32),
    Color(RenderColor),
    Room(String),
    Bold(bool),
    Italic(bool),
}

pub struct Render<'a, 'b: 'a> {
    webview: &'a mut WebView<'b, GameState>,
}

impl<'x, 'y> Render<'x, 'y> {
    /// Return a render object with the webview passed to this function
    pub fn from_webview<'a, 'b>(input: &'a mut WebView<'b, GameState>) -> Render<'a, 'b> {
        Render { webview: input }
    }

    /// This updates the room displayed in the top left corner
    pub fn set_room(&mut self, room: Vec<RenderCommand>) {
        let mut room_name = String::new();

        // create the roomname
        for command in room.iter() {
            match command {
                RenderCommand::Text(s) => room_name.push_str(&s),
                _ => (), //Rooms can only be text
            }
        }

        // create the json
        let json = json!({
            "action": "room",
            "roomName": room_name
        }).to_string();

        // send it to the frontend
        self.webview
            .eval(&format!("addCommand({})", web_view::escape(&json)));
    }

    pub fn write(&mut self, text: Vec<RenderCommand>) {
        for command in text.iter() {
            let mut json = String::new();

            // create the json
            match command {
                RenderCommand::Text(s) => {
                    json = json!({
                        "action": "write",
                        "text": s
                    }).to_string();
                }
                RenderCommand::Sleep(t) => {
                    json = json!({
                        "action": "sleep",
                        "duration": t * 1000.0 // Convert seconds into milliseconds
                    }).to_string();
                }
                RenderCommand::Color(color) => {
                    let mut color_string = String::new();

                    match color {
                        RenderColor::Standard => color_string = String::from(""),
                        RenderColor::Red => color_string = String::from("red"),
                        RenderColor::Green => color_string = String::from("green"),
                        RenderColor::Blue => color_string = String::from("blue"),
                        RenderColor::Orange => color_string = String::from("orange"),
                        RenderColor::Purple => color_string = String::from("purple"),
                    }

                    json = json!({
                        "action": "color",
                        "color": color_string
                    }).to_string();
                }
                RenderCommand::Room(name) => {
                    self.set_room(vec![RenderCommand::Text(name.to_string())]);
                }
                _ => (), //TODO: implement the other variants
            }

            // send the json to the frontend
            self.webview
                .eval(&format!("addCommand({})", web_view::escape(&json)));
        }

        print!{"\n"};
    }

    /// This shows the user a quit button to kill the application
    pub fn show_exit(&mut self) {
        // create json
        let json = json!({
            "action": "end"
        }).to_string();

        // send it to the frontend
        self.webview
            .eval(&format!("addCommand({})", web_view::escape(&json)));
    }

    /// This should be used for internal errors of the gameegine. It displays the error string the
    /// user with the quit button underneath.
    pub fn show_error(&mut self, error: &str) {
        // create json
        let json = json!({
            "action": "error",
            "text": error
        }).to_string();

        // send it to the frontend
        self.webview
            .eval(&format!("addCommand({})", web_view::escape(&json)));
    }

    /// Displaces the options the user so he can choose one
    pub fn show_decission(&mut self, choices: Vec<Vec<RenderCommand>>) {
        let mut choice_list: Vec<String> = Vec::new();

        for choice in choices.iter() {
            let mut tmp = String::new();

            for command in choice.iter() {
                match command {
                    RenderCommand::Text(s) => tmp.push_str(&s),
                    _ => (), //Only text is sopported right now
                }
            }

            choice_list.push(tmp.clone());
        }

        // create the json
        let json = json!({
                "action": "decission",
                "optionList": choice_list
            }).to_string();

        // send the json the frontend
        self.webview
            .eval(&format!("addCommand({})", web_view::escape(&json)));
    }
}
