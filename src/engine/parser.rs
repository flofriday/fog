use engine::render::{RenderColor, RenderCommand};
use engine::GameState;

/// Those are the states in which the parser can exit
pub enum ParseResult {
    Continue,
    NextRoom, // Also when a new room from another file got loaded
    Error(String),
    Exit,
}

/// This function parses the input string, modifys the GameState if needed and returns with the
/// ParseResult
// TODO: rewrite everything with nom, because I have no f*cking idea how this 
// really works, even tough I have written it.
pub fn parse(state: &mut GameState, input: &str) -> (ParseResult, Vec<RenderCommand>) {
    let mut commands = Vec::new();
    let mut result = ParseResult::Continue;

    let mut start_pos = 0;
    let mut is_text = true;
    for (pos, char) in input.chars().enumerate() {
        if is_text == true && char == '<' {
            // TODO check if is empty
            commands.push(RenderCommand::Text(input[start_pos..pos].to_string()));
            start_pos = pos + 1;
            is_text = false;
        }
        if is_text == false && char == '>' {
            let part = input[start_pos..pos].to_string();
            let mut part: Vec<&str> = part.split_whitespace().collect();
            if part.is_empty() {
                return (
                    ParseResult::Error(format!(
                        "Tried to parse \"{}\", but there is a empty command.",
                        input
                    )),
                    Vec::new(),
                );
            }
            let command = part.remove(0).to_string();
            let command = command.to_uppercase();
            let attributes = part;

            // Detect command, check and execute
            if command == "GO" {
                if attributes.len() < 1 {
                    return (
                        ParseResult::Error(format!(
                            "Expected at least 1 attribute for the GO command.\n\
                             Got {} when tried to parse: \"{}\"",
                            attributes.len(),
                            input
                        )),
                        Vec::new(),
                    );
                }

                // Assemble all attributes to the Room name
                let mut name = String::new();
                for attribute in attributes.iter() {
                    name.push_str(&attribute);
                }

                let mut found = false;
                for (id, room) in state.current_room_list.iter().enumerate() {
                    if room.name == name {
                        state.current_room_id = id as u32;
                        found = true;
                        break;
                    }
                }

                // check if room was found
                if found == false {
                    return (
                        ParseResult::Error(format!(
                            "Could not find room \"{}\" in {:?}\n\
                             Happened, when tried to parse: \"{}\"",
                            name, state.current_file, input
                        )),
                        Vec::new(),
                    );
                }

                result = ParseResult::NextRoom;
                break;
            } else if command == "ROOM" {
                if attributes.len() < 1 {
                    return (
                        ParseResult::Error(format!(
                            "Expected at least 1 attribute for the GO command.\n\
                             Got {} when tried to parse: \"{}\"",
                            attributes.len(),
                            input
                        )),
                        Vec::new(),
                    );
                }

                // Assemble all attributes to the Room name
                let mut name = String::new();
                for attribute in attributes.iter() {
                    name.push_str(&attribute);
                }

                commands.push(RenderCommand::Room(name));
            } else if command == "LOAD" {
                if attributes.len() != 1 {
                    return (
                        ParseResult::Error(format!(
                            "Expected exactly 1 attribute for the LOAD command.\n\
                             Got {} when tried to parse: \"{}\"",
                            attributes.len(),
                            input
                        )),
                        Vec::new(),
                    );
                }

                // handle if file not found
                match state.load_file(attributes[0]) {
                    Err(s) => {
                        return (ParseResult::Error(s), Vec::new());
                    }
                    _ => ()
                };

                result = ParseResult::NextRoom;
                break;
            } else if command == "END" {
                //TODO: check number of arguments
                result = ParseResult::Exit;
                break;
            } else if command == "SLEEP" {
                if attributes.len() != 1 {
                    return (
                        ParseResult::Error(format!(
                            "Expected exactly 1 attribute for the SLEEP command.\n\
                             Got {} when tried to parse: \"{}\"",
                            attributes.len(),
                            input
                        )),
                        Vec::new(),
                    );
                }

                let sleep_time: f32 = match attributes[0].parse() {
                    Ok(t) => t,
                    Err(_) => {
                        return (
                            ParseResult::Error(format!(
                            "Unable to parse the SLEEP command\"{}\", when trying to parse: \"{}\"",
                            command, input
                        )),
                            Vec::new(),
                        )
                    }
                };

                commands.push(RenderCommand::Sleep(sleep_time));
            } else if command == "COLOR" {
                if attributes.len() != 1 {
                    return (
                        ParseResult::Error(format!(
                            "Expected exactly 1 attribute for the SLEEP command.\n\
                             Got {} when tried to parse: \"{}\"",
                            attributes.len(),
                            input
                        )),
                        Vec::new(),
                    );
                }

                let color_string = attributes[0].to_lowercase();
                let color: RenderColor;
                if color_string == "default" || color_string == "standard" {
                    color = RenderColor::Standard;
                } else if color_string == "red" {
                    color = RenderColor::Red;
                } else if color_string == "green" {
                    color = RenderColor::Green;
                } else if color_string == "blue" {
                    color = RenderColor::Blue;
                } else if color_string == "orange" {
                    color = RenderColor::Orange;
                } else if color_string == "purple" {
                    color = RenderColor::Purple;
                } else {
                    return (
                        ParseResult::Error(format!(
                            "Found unknown color \"{}\", when trying to parse: \"{}\"",
                            attributes[0], input
                        )),
                        Vec::new(),
                    );
                }

                commands.push(RenderCommand::Color(color));
            } else {
                return (
                    ParseResult::Error(format!(
                        "Found unknown command \"{}\", when trying to parse: \"{}\"",
                        command, input
                    )),
                    Vec::new(),
                );
            }

            start_pos = pos + 1;
            is_text = true;
        }

        // Last iteration
        if pos + 1 >= input.len() {
            if is_text == false {
                return(ParseResult::Error(format!("Tried to parse \"{}\", but there is opening command bracket which never got closed.", input)), Vec::new());
            }
            commands.push(RenderCommand::Text(input[start_pos..].to_string()));
        }
    }

    //println!("{:?}", commands);
    (result, commands)
}
