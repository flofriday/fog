use engine::item::Item;

#[derive(Debug, Clone)]
pub struct Choice {
    pub name: String,
    pub action: String,
}

#[derive(Debug, Clone)]
pub struct Room {
    pub name: String,
    pub description: String,
    pub items: Vec<Item>,
    pub decision: Vec<Choice>,
}
