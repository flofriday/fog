use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use web_view::*;
use yaml_rust::YamlLoader;

mod item;
mod parser;
mod render;
mod room;

use self::item::Item;
use self::parser::{parse, ParseResult};
use self::render::{Render, RenderCommand};
use self::room::{Choice, Room};

/// This struct saves the current state the game is in right now
pub struct GameState {
    current_file: PathBuf,
    current_room_id: u32,
    current_room_list: Vec<Room>,
    inventory_list: Vec<Item>,
    item_list: Vec<Item>,
}

impl GameState {
    /// This file loads the file given and parses all of the rooms in it.
    pub fn load_file<T: Into<PathBuf>>(&mut self, path: T) -> Result<(), String> {
        let path: PathBuf = path.into();

        // create file object
        let mut f = match File::open(&path) {
            Ok(file) => file,
            Err(_) => return Err(format!("Unable to open file: {:?}", &path)),
        };

        // read file into String
        let mut buffer = String::new();
        match f.read_to_string(&mut buffer) {
            Ok(_) => (),
            Err(_) => return Err(format!("The file {:?} does not contain valid UTF-8", &path)),
        };

        // parse file content into yaml
        let docs = match YamlLoader::load_from_str(&buffer) {
            Ok(yaml) => yaml,
            Err(e) => {
                return Err(format!(
                    "The file {:?} does not contain valid YAML code.\nExact Error: {:?}",
                    &path, e
                ))
            }
        };
        let doc = &docs[0];

        // parse yaml into a roomlist
        let mut room_list = Vec::new();
        for obj in doc.as_vec().unwrap().iter() {
            let obj = &obj["room"];

            let mut decision = Vec::new();
            if obj["decision"].is_null() == false && obj["decision"].is_badvalue() == false {
                for choice in obj["decision"].as_vec().unwrap().iter() {
                    decision.push(Choice {
                        name: choice["name"].as_str().unwrap().to_string(),
                        action: choice["action"].as_str().unwrap().to_string(),
                    })
                }
            }

            room_list.push(Room {
                name: obj["name"].as_str().unwrap().to_string(),
                description: obj["description"].as_str().unwrap().to_string(),
                items: Vec::new(), // TODO: implement items
                decision,
            })
        }

        // Update the GameState object
        self.current_file = path.into();
        self.current_room_list = room_list;
        self.current_room_id = 0;
        Ok(())
    }

    /// This function creates a new GameState object with the correct file already set,
    /// but it doesn't load the file.
    pub fn from_file<T: Into<PathBuf>>(path: T) -> GameState {
        GameState {
            current_file: path.into(),
            current_room_id: 0,
            current_room_list: Vec::new(),
            item_list: Vec::new(),
            inventory_list: Vec::new(),
        }
    }
}

/// This function runs the current file and room.
fn run(state: &mut GameState, mut render: Render) {
    'rooms: loop {
        let room = state.current_room_list[state.current_room_id as usize].clone();

        // Room Name
        let (result, name) = parse(state, &room.name);
        render.set_room(name);
        match result {
            ParseResult::Continue => (),
            ParseResult::NextRoom => {
                render.show_error(&format!(
                    "It is forbidden to change the room in the room name.\n{:?} -> \"{}\"",
                    state.current_file, room.name,
                ));
                return;
            }
            ParseResult::Error(e) => {
                render.show_error(&e);
                return;
            }
            ParseResult::Exit => {
                render.show_exit();
                return;
            }
        }

        // Room Description
        let (result, description) = parse(state, &room.description);
        render.write(description);
        match result {
            ParseResult::Continue => (),
            ParseResult::NextRoom => continue,
            ParseResult::Error(e) => {
                render.show_error(&e);
                return;
            }
            ParseResult::Exit => {
                render.show_exit();
                return;
            }
        }

        // Room Decision
        if room.decision.is_empty() {
            render.show_error(&format!(
                "There is no decision in room: {:?} -> \"{}\"",
                state.current_file, room.name
            ));
            return;
        }

        let mut choice_descriptions = Vec::new();
        for choice in room.decision.iter() {
            let (result, choice) = parse(state, choice.name.as_str());
            choice_descriptions.push(choice);
            match result {
                //TODO: forbid changing room/file
                ParseResult::Continue => (),
                ParseResult::NextRoom => {
                    render.show_error(
                        "It is forbidden to change the room in the choice description.",
                    );
                }
                ParseResult::Error(e) => {
                    render.show_error(&e);
                    return;
                }
                ParseResult::Exit => {
                    render.show_exit();
                    return;
                }
                _ => (),
            }
        }

        render.show_decission(choice_descriptions);
        break;
    }
}

/// This function gets called when the games starts up.
/// It does some setting up and then calles the `run` function
pub fn start(state: &mut GameState, webview: &mut web_view::WebView<GameState>) {
    // We need to load the first file
    let mut render = Render::from_webview(webview);
    let path = state.current_file.clone();
    match state.load_file(path) {
        Err(s) => {
            render.show_error(&s);
            return;
        }
        _ => (),
    };

    run(state, render);
}

/// This function gets called after the user made a decision.
/// It first parses the user choice and then calls the `run` function
pub fn continue_with_choice(
    state: &mut GameState,
    webview: &mut web_view::WebView<GameState>,
    choice: u64,
) {
    // check current option and parse it
    let mut render = Render::from_webview(webview);
    let room = state.current_room_list[state.current_room_id as usize].clone();
    let (result, text) = parse(state, &room.decision[choice as usize].action);
    render.write(text);
    match result {
        ParseResult::Continue => (),
        ParseResult::NextRoom => (),
        ParseResult::Error(e) => {
            render.show_error(&e);
            return;
        }
        ParseResult::Exit => {
            render.show_exit();
            return;
        }
    }

    // continue normally with the game
    /*render.show_error("Relax, nothing went wrong, This is just a test\nwith two lines");
    render.set_room(vec![RenderCommand::Text(String::from("New Room"))]);
    render.write(vec![
        RenderCommand::Text(String::from("Thank you for voting.")),
        RenderCommand::Sleep(3.0),
        RenderCommand::Text(String::from("\nHow do you like the game?")),
    ]);
    render.show_decission(vec![
        vec![RenderCommand::Text(String::from("I like it"))],
        vec![RenderCommand::Text(String::from("It is fine"))],
    ]);
    render.show_exit();
    render.show_exit();*/
    run(state, render);
}
